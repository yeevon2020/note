package com.demoapp.alcodesonboard.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.afollestad.materialdialogs.MaterialDialog;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.database.entities.MyNote;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModelFactory;
import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyNoteDetailFragment extends Fragment {

    public static final String TAG = MyNoteDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_NOTE_ID = "ARG_LONG_MY_NOTE_ID";

    @BindView(R.id.edittext_title)
    protected TextInputEditText mEditTextTitle;

    @BindView(R.id.edittext_content)
    protected TextInputEditText mEditTextContent;

    private Unbinder mUnbinder;
    private MyNotesViewModel mViewModel;
    private Long mMyNoteId = 0L;

    public MyNoteDetailFragment() {
    }

    public static MyNoteDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_NOTE_ID, id);

        MyNoteDetailFragment fragment = new MyNoteDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_note_detail, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

        if (args != null) {
            mMyNoteId = args.getLong(ARG_LONG_MY_NOTE_ID, 0);
        }

        initView();
        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        mMyNoteId = 0L;

        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_my_note_detail, menu);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);

        // TODO change menu label "Save" to "Create" for new note.
        MenuItem menuItem = menu.findItem(R.id.menu_save);

        if (mMyNoteId == 0) {
            menuItem.setTitle("Create");
        } else {
            menuItem.setTitle("Save");
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        // TODO check email and password is blank or not.
        String title = mEditTextTitle.getText().toString();
        String content = mEditTextContent.getText().toString();

        if (itemId == R.id.menu_save) {

            if (title.isEmpty()) {
                Toast.makeText((getActivity()), "Please enter title", Toast.LENGTH_LONG).show();
            } else if (content.isEmpty()) {
                Toast.makeText((getActivity()), "Please enter content.", Toast.LENGTH_LONG).show();
            } else {
                // TODO show confirm dialog before continue.
                new MaterialDialog.Builder(super.getContext())
                        .title("Confirm Note")
                        .content("Do you want to continue?")
                        .positiveText("Yes")
                        .negativeText("No")
                        .onNegative(null)
                        .onPositive((dialog, which) -> {

                            // TODO BAD practice, should move Database operations to Repository.
                            if (mMyNoteId > 0) {
                                // Update record.
                                mViewModel.editNote(mMyNoteId, title, content);
                                Toast.makeText(getActivity(), "Note saved.", Toast.LENGTH_SHORT).show();

                            } else {
                                // Create record.
                                mViewModel.addNote(title, content);
                                Toast.makeText(getActivity(), "Note created.", Toast.LENGTH_SHORT).show();
                            }
                            super.getActivity().finish();
                            //getActivity().finish();
                        })
                        .show();

                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        // TODO BAD practice, should move Database operations to Repository.
    }

    private void initViewModel() {
        mViewModel = new ViewModelProvider(this, new MyNotesViewModelFactory(getActivity().getApplication())).get(MyNotesViewModel.class);
        mViewModel.getMyNoteLiveData().observe(getViewLifecycleOwner(), new Observer<MyNote>() {
            @Override
            public void onChanged(MyNote myNote) {
                if (myNote != null) {
                    // Record found, fill to UI.
                    mEditTextTitle.setText(myNote.getTitle());
                    mEditTextContent.setText(myNote.getContent());
                } else {
                    // Record not found.
                    Toast.makeText(getActivity(), "Note not found.", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
            }
        });

        if (mMyNoteId > 0) {
            mViewModel.loadMyNoteById(mMyNoteId);
        }
    }
}
