package com.demoapp.alcodesonboard.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyNoteDetailActivity;
import com.demoapp.alcodesonboard.adapters.MyNotesAdapter;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModel;
import com.demoapp.alcodesonboard.viewmodels.MyNotesViewModel.MyNotesViewModelFactory;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyNotesFragment extends Fragment implements MyNotesAdapter.Callbacks {

    public static final String TAG = MyNotesFragment.class.getSimpleName();

    @BindView(R.id.recyclerview)
    protected RecyclerView mRecyclerView;

    private Unbinder mUnbinder;
    private MyNotesAdapter mAdapter;
    private MyNotesViewModel mViewModel;

    public MyNotesFragment() {
    }

    public static MyNotesFragment newInstance() {
        return new MyNotesFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_notes, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel(super.getView());
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fragment_my_notes, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_add) {
            startActivity(new Intent(getActivity(), MyNoteDetailActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onListItemClicked(MyNotesAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), MyNoteDetailActivity.class);
        intent.putExtra(MyNoteDetailActivity.EXTRA_LONG_MY_NOTE_ID, data.id);

        startActivity(intent);
    }

    @Override
    public void onDeleteButtonClicked(MyNotesAdapter.DataHolder data) {
        mViewModel.deleteNote(data.id);
    }

    private void initView() {
        mAdapter = new MyNotesAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initViewModel(View view) {
        mViewModel = new ViewModelProvider(this, new MyNotesViewModelFactory(getActivity().getApplication())).get(MyNotesViewModel.class);
        mViewModel.getMyNotesAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<MyNotesAdapter.DataHolder>>() {

            @Override
            public void onChanged(List<MyNotesAdapter.DataHolder> dataHolders) {
                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();

                // TODO check dataHolders has data or not.
                // TODO show list if have, otherwise show label: "No data"
                if (dataHolders.isEmpty()) {
                    RecyclerView cycleview = view.findViewById(R.id.recyclerview);
                    TextView errorView = view.findViewById(R.id.noData);

                    cycleview.setVisibility(View.GONE);
                    errorView.setVisibility(View.VISIBLE);
                    // TextView view = (TextView) findViewById();
                } else {
                    RecyclerView cycleview = view.findViewById(R.id.recyclerview);
                    TextView errorView = view.findViewById(R.id.noData);
                    errorView.setVisibility(View.GONE);
                    cycleview.setVisibility(View.VISIBLE);
                }
            }
        });

        // Load data into adapter.
        mViewModel.loadMyNotesAdapterList();
    }
}